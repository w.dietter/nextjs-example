import axios from 'axios';

const Posts = ({ comments, postId }) => {
  return (
    <>
      <h1>Posts Page for post {postId}</h1>
      <ul>
        {comments.map(comment => (
          <li
            key={comment.id}
            style={{ listStyle: 'none', borderBottom: '1px solid black' }}
          >
            <Comment {...comment} />
          </li>
        ))}
      </ul>
    </>
  );
};

const Comment = ({ email, body }) => (
  <div>
    <h5>{email}</h5>
    <p>{body}</p>
  </div>
);

Posts.getInitialProps = async ({ query }) => {
  const response = await axios.get(
    `https://jsonplaceholder.typicode.com/comments?postId=${query.postId}`
  );
  return { comments: response.data, ...query };
};

export default Posts;
