import React, { Component } from 'react';
import axios from 'axios';
import Link from 'next/link';

const url = `https://jsonplaceholder.typicode.com/posts`;

const Index = ({ posts }) => {
  return (
    <div>
      <h1>Index Page</h1>
      <ul>
        {posts.map(post => (
          <li key={post.id}>
            <Link href={`/posts?postId=${post.id}`} as={`/p/${post.id}`}>
              <a>{post.title}</a>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

Index.getInitialProps = async () => {
  let response;
  try {
    response = await axios.get(url);
  } catch (err) {
    console.log(err);
  }
  return { posts: response.data };
};

export default Index;
